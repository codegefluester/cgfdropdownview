//
//  CGFDropdownView.m
//  CGFDropdownView
//
//  Created by Björn Kaiser on 24.01.14.
//  Copyright (c) 2014 Björn Kaiser. All rights reserved.
//

#import "CGFDropdownView.h"

@interface CGFDropdownView ()

@property (strong, nonatomic) NSMutableArray *dropdownItems;
@property (readwrite) NSInteger selectedItemIndex;

@end

@implementation CGFDropdownView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
        _dropdownItems = [NSMutableArray new];
    }
    return self;
}

- (instancetype) initWithItems:(NSArray*)items
{
    self = [self initWithFrame:CGRectMake(0, 0, 300, 40)];
    
    if(self)
    {
        if(items != nil && items.count > 0)
        {
            for (NSString *item in items)
            {
                CGFDropdownViewItem *tmpItem = [[CGFDropdownViewItem alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
                [_dropdownItems addObject:tmpItem];
            }
            
            _selectedItemIndex = 0;
        }
    }
    
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
}

@end
