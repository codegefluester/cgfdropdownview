//
//  CGFDropdownViewItem.h
//  CGFDropdownView
//
//  Created by Björn Kaiser on 24.01.14.
//  Copyright (c) 2014 Björn Kaiser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CGFDropdownViewItem : UIView

@end
