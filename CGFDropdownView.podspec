Pod::Spec.new do |s|
  s.name         = "CGFDropdownView"
  s.version      = "0.0.1"
  s.summary      = "Add a drop down menu to your app, similar like in the Facebook app for iOS"
  s.homepage     = "http://github.com/codegefluester/CGFDropdownView"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author             = { "codegefluester" => "bjoern@codegefluester.de" }
  s.platform     = :ios, '7.0'
  s.source       = { :git => "https://bitbucket.org/codegefluester/cgfdropdownview.git", :tag => "0.0.1" }
  s.source_files  = '*.{h,m}'
  s.exclude_files = '*.md'
  s.requires_arc = true
end
