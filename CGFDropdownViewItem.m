//
//  CGFDropdownViewItem.m
//  CGFDropdownView
//
//  Created by Björn Kaiser on 24.01.14.
//  Copyright (c) 2014 Björn Kaiser. All rights reserved.
//

#import "CGFDropdownViewItem.h"

@implementation CGFDropdownViewItem

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

@end
