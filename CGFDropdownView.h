//
//  CGFDropdownView.h
//  CGFDropdownView
//
//  Created by Björn Kaiser on 24.01.14.
//  Copyright (c) 2014 Björn Kaiser. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGFDropdownViewItem.h"

@interface CGFDropdownView : UIView
{

}

@property (readonly) NSInteger selectedItemIndex;

- (instancetype) initWithItems:(NSArray*)items;

@end
